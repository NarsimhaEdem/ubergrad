package com.google.firebase.codelab.friendlychat;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by narsimha on 28-Feb-17.
 */

public class SignUpActivity extends AppCompatActivity {
    EditText firstName,lastName,email,mobile,password,confirm_password;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);


        firstName=(EditText)findViewById(R.id.first_name);
        lastName=(EditText)findViewById(R.id.last_name);
        email=(EditText)findViewById(R.id.email);
        mobile=(EditText)findViewById(R.id.mobile_number);
        password=(EditText)findViewById(R.id.password);
        confirm_password  =(EditText)findViewById(R.id.confirm_password);
        button=(Button)findViewById(R.id.signupBtn) ;



        final User userInfo=new User();

       // final User userInfo=new User();

    String fName= firstName.getText().toString().trim();
    String lName=  lastName.getText().toString().trim();
    String mail=email.getText().toString().trim();
    String mob=mobile.getText().toString().trim();
    String pwd=password.getText().toString().trim();


    userInfo.setFirstname(fName);
    userInfo.setLastname(lName);
    userInfo.setEmail(mail);
    userInfo.setMobile(mob);
    userInfo.setPassword(pwd);
    button.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {


            DataManager dataManager = DataManager.getDataManager();
            //get store details by using present loaction
            dataManager.createUser(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    User u = response.body();
                    String msg = u.getMeasage();
                    Log.d("Msg", msg);
                    Toast.makeText(SignUpActivity.this, msg, Toast.LENGTH_LONG).show();
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {

                }
            }, userInfo);
        }
    });

}


    }

