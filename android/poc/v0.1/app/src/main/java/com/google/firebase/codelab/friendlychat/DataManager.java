package com.google.firebase.codelab.friendlychat;

import android.content.Context;
import android.media.Image;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by stplguest on 2/11/16.
 */
public class DataManager {
    private static DataManager dataManager = null;
   // private static final String BASE_URL = "http://192.168.131.1:4000";
  private static final String BASE_URL = "http://192.168.91.105:7000";





    private Retrofit retrofit;

    private DataManager() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        // add your other interceptors …

        // add logging as last interceptor
        httpClient.addInterceptor(logging);  // <-- this is the important line!

        retrofit = new Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();
    }

    public static DataManager getDataManager() {
        if(dataManager == null) {
            dataManager = new DataManager();
        }
        return dataManager;
    }

    public void userLogin(Callback<User> cb,User user) {
        UserService apiService = retrofit.create(UserService.class);
        Call<User> call = apiService.userLogin(user);
        call.enqueue(cb);
    }
    public void createUser(Callback<User> cb,User userInfo) {
        UserService apiService = retrofit.create(UserService.class);
        Call<User> call = apiService.createUser(userInfo);
        call.enqueue(cb);
    }

}
