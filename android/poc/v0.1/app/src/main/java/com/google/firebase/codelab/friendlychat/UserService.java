package com.google.firebase.codelab.friendlychat;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by stplguest on 2/11/16.
 */
public interface UserService {


//    @POST("/firebaseChat/login")
//    Call<User> userLogin(@Field("email") String email, @Field("password") String password);
@POST("/firebaseChat/login")
Call<User> userLogin( @Body User user);

    @POST("/firebaseChat/user/register")
    Call<User> createUser( @Body User user);



}